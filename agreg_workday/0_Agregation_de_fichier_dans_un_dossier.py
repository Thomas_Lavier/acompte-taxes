import glob
import pandas as pd
import numpy as np
import datetime
import re
import Constant

repo = Constant.repo()
sub_repo = Constant.sub_repo()
fichier = Constant.fichier()

columns = ["Type","Compte client","Millesime Prix","Offre_BP","Categorie de produit","Rate Plan Charge ID","Rate Plan Charge Name","Canal de distribution","Code TVA","Prix unitaire","Quantites","Numero transaction","Compte comptable debit","Montant","Compte comptable credit","Date de transaction","Puissance souscrite ","Numero paiement Zuora","Numero paiement Gateway","Type de facturation","FTA","Calendrier Fournisseur","Produit Analytique"]


### Liste des fichiers à comparer
Files = glob.glob(repo+'/'+sub_repo+'/'+fichier, recursive=True)

## Créer DF Vide

Fichier_Final = pd.DataFrame()

for X_Files in Files:

	Export = pd.read_csv(X_Files, sep=';', header=None, encoding='UTF-8', dtype=object )
	Fichier_Final = Fichier_Final.append(Export, ignore_index=True, sort=False)


Fichier_Final[10] = np.where(Fichier_Final[0] == 'TaxationItem', 1 ,Fichier_Final[10] )

Fichier_Final.to_csv(repo+'/'+"1_Fichier_Final_Rattrappage_"+sub_repo+".csv", sep=';', encoding='UTF-8', index=False, header=None)
print('generation 1_Fichier_Final_Rattrappage.csv ')



