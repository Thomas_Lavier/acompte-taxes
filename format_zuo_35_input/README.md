# Script format_zuo_35_call

## Description
Découpe le body d'un fichier json au format de l'entrée de ZUO_35_Update_with_file, pour en faire des lots avec un nombre de records arbitraire (précisé en argument).

## Requirements

Python3.xx 
Tous les imports font partie de la librairie standard Python :
* itertools
* json
* sys

## Utilisation

    python3 format_zuo_35_input chemin_entree prefixe_sortie [taille_lots]

* **chemin_entree** : chemin vers le fichier à lotir
* **prefixe_sortie** : préfixe pour les noms de fichiers de sortie. Les fichiers de sorties seront de la forme *{prefixe_sortie}00n.json*
* **taille_lots** (optionnel) : nombre de records par lot, par défaut 1000

### Exemple

    python3 format_zuo_35_call.py ZUO_35_05082021.json.archive ZUO_35_ 1000

**NB** le nom du fichier de sortie est hard-codé pour numéroter les fichier de sortie sur 3 chiffres, pour modifier le nombre de chiffres, modifier la constante *FILENAME_DIGITS* directement dans le code.
