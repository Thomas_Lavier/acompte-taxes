#!/usr/bin/python3
import itertools
import json
import sys


FILENAME_DIGITS = 3


# Initialisation basée sur les arguments
if len(sys.argv) < 3 or len(sys.argv) > 4:
    print("Expected 2 or 3 arguments : \ninput_path output_prefix [chunk_size]")
    sys.exit(2)

input_path,prefix = sys.argv[1],sys.argv[2]

if len(sys.argv)==4:
    try:
        chunk_size = int(sys.argv[3])
        if chunk_size < 1:
            raise ValueError
    except ValueError:
        print('Expected greater than 1 integer as third argument (chunk_size)')
        sys.exit(2)
else:
    chunk_size = 1000

output_path = prefix + '{0}.json'


with open(input_path) as f:
  data = json.load(f)

records = data['action']['records']

output_path = prefix + '{0}.json'

records_in_chunks = [dict(itertools.islice(records.items(), i,i+chunk_size)) for i in range(0, len(records.keys()), chunk_size)]

i=1
for chunk in records_in_chunks:
    res = {
        'action' : {
            'type': 'update',
            'records': chunk
        }
    }
    with open(output_path.format(str(i).rjust(FILENAME_DIGITS,'0')),'a+') as output_file:
        output_file.write(json.dumps(res))
    
    i=i+1


