# -------------------------
# --- Obsolete not used ---
# --- Confirmed by Flavien-
# -------------------------

import glob
import pandas as pd
import numpy as np

import Constant

repo = Constant.repo()

extract_ppi_zuora = glob.glob(repo + '/liste ppi/1_Fichier_recap_ppi.csv')
print(extract_ppi_zuora)
extract_ppi_zuora = pd.read_csv(extract_ppi_zuora[0], sep=';', encoding='UTF-8')
extract_ppi_zuora = extract_ppi_zuora.rename(columns={'id':'Transaction Number'})

extract_ppi_zuora = extract_ppi_zuora.loc[ (extract_ppi_zuora['TrasferredTOAccountingLast3Month__c'] == True) | (extract_ppi_zuora['TrasferredTOAccountingLastMonth__c'] == True) ,:]

extract_ppi_zuora['TrueTrue'] = np.where(extract_ppi_zuora['TrasferredTOAccountingLast3Month__c'] == extract_ppi_zuora['TrasferredTOAccountingLastMonth__c'], 'OK', 'KO')

#concat_files_tax = glob.glob(repo + '/1_Fichier_Final_Acompte_Full.csv')
concat_files_tax = glob.glob(repo + '/2_Fichier_Final_Acompte_Full.csv')
print(concat_files_tax)
concat_files_tax = pd.read_csv(concat_files_tax[0], sep=';', encoding='UTF-8')

concat_files_tax['Mensu_2020-11-04'] = concat_files_tax['Mensu_2020-11-04'].str.replace(",",".").str.encode('ascii','ignore').astype(float)
concat_files_tax['Mensu_2020-12-02'] = concat_files_tax['Mensu_2020-12-02'].str.replace(",",".").str.encode('ascii','ignore').astype(float)
concat_files_tax['Mensu_2021-01-04'] = concat_files_tax['Mensu_2021-01-04'].str.replace(",",".").str.encode('ascii','ignore').astype(float)

concat_files_tax['Total_fin_decembre'] = concat_files_tax['Mensu_2020-11-04'] + concat_files_tax['Mensu_2020-12-02'] + concat_files_tax['Mensu_2021-01-04']


Fichier_Final = concat_files_tax.merge(extract_ppi_zuora, how='outer', on='Transaction Number')

Fichier_Final.to_csv("1_extract_zuora_versus_zuo_38.csv", sep=';', encoding='UTF-8', index=False, decimal=',')

