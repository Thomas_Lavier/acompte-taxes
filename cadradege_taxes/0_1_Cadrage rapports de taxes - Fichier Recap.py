# -------------------------
# --- Main script
# --- Always run
# -------------------------

import glob
import pandas as pd
import numpy as np
import re
import Constant

# Récupération de la variable pour savoir ou est le dossier avec les pythons en local
repo = Constant.repo()
localOutput = Constant.localOutput()
localTransco = Constant.transco_config()

# Prends les fichiers du repository dont le répertoire commence par 202 et les fichiers contenant Export_ et .csv
Files = glob.glob(repo + '/202*/Export_*.csv', recursive=True)

# Créer une dataframe vide qui va être réutilisé dans la boucle ci dessous
Fichier_Final = pd.DataFrame(
    columns=['jours_extraction', 'Categorie', 'Sous_Categorie', 'code SI', 'Nom_Fichier', 'Montant en Euros'])

# Récupère la table de transcodification qui permet de modifier le nom du compte dans un format comparable aux
# fichiers des taxes
TC_Sous_Categorie = pd.read_csv(localTransco + '/0_TC_Nom_du_compte_x_Sous_Categorie.csv', sep=';', encoding='UTF-8')

# Boucle qui traite fichier par fichier
for X_Files in Files:
    print(X_Files)
    # .split("\\") permet de spliter le Chemin en Date et nom du Fichier
    url = X_Files.split("\\")
    Date_repertoire = url[1]
    Nom_Fichier = url[2]
    # print(Date_repertoire)
    Split_nom_fichier = re.split('Export_|_202', Nom_Fichier)
    Categorie = Split_nom_fichier[1]
    # print(Categorie)

    # Travail sur les fichiers

    # Files: Acompte_Details & Acompte_Summary
    if Categorie == 'Acompte_Details' or Categorie == 'Acompte_Summary':
        # lis le fichier sous forme de table
        Export = pd.read_csv(X_Files, sep=';', encoding='UTF-8')
        # Récupération du champ "sous catégorie" en fonction du nom du compte qui est dans le fichier
        Export = Export.merge(TC_Sous_Categorie, on='Nom du compte', how='left')
        Export = Export.drop(columns=['Nom du compte'])

        # Montant_en_euros = total des montants en euros du fichier
        Montant_en_euros = Export['Montant en Euros'].sum().round(2)

        # 1 -Création de la ligne total
        # Append permet d'ajouter des lignes dans le fichier final
        Fichier_Final = Fichier_Final.append(
            {'jours_extraction': Date_repertoire, 'Categorie': Categorie, 'Sous_Categorie': 'Total',
             'code SI': 'Acompte', 'Nom_Fichier': Nom_Fichier, 'Montant en Euros': Montant_en_euros}, ignore_index=True)

        # 2 - Création des totaux par sous catégories
        Export = Export.groupby(['Sous_Categorie'], as_index=False).sum()
        # Récupération des colonnes sous catégorie + euros
        Export = Export[['Sous_Categorie', 'Montant en Euros']]
        # Complément d'informations
        Export['jours_extraction'] = Date_repertoire
        Export['Categorie'] = Categorie
        Export['code SI'] = 'Acompte'
        Export['Nom_Fichier'] = Nom_Fichier
        # print(Export)

        # Append permet d'ajouter des lignes dans le fichier final
        Fichier_Final = Fichier_Final.append(Export, ignore_index=True)

    # Files: CSPE, CTA_ELEC, CTA_GAZ, TCFE, TICGN
    else:
        Export = pd.read_csv(X_Files, sep=',', encoding='UTF-8')

        # Pour la partie code si = acompte
        Export_Acompte = Export.loc[Export['code SI'] == 'Acompte', :] # ici tu l'utilises comme un group by mais après je ne vois pas où tu append la sous catégorie ?
        Montant_en_euros = Export_Acompte['Montant en Euros'].sum().round(2)

        # Append permet d'ajouter des lignes dans le fichier final
        Fichier_Final = Fichier_Final.append(
            {'jours_extraction': Date_repertoire, 'Categorie': Categorie, 'Nom_Fichier': Nom_Fichier,
             'code SI': 'Acompte', 'Montant en Euros': Montant_en_euros}, ignore_index=True)

        # Pour la partie code si = factu
        Export_Factu = Export.loc[Export['code SI'] == 'Factu', :]
        Montant_en_euros = Export_Factu['Montant en Euros'].sum().round(2)

        # Append permet d'ajouter des lignes dans le fichier final
        Fichier_Final = Fichier_Final.append(
            {'jours_extraction': Date_repertoire, 'Categorie': Categorie, 'Nom_Fichier': Nom_Fichier,
             'code SI': 'Factu', 'Montant en Euros': Montant_en_euros}, ignore_index=True)

# print(Fichier_Final)

# Pour les fichiers qui ne sont pas Acompte detail ou acompte summary, la sous categorie = categorie
Fichier_Final['Sous_Categorie'] = np.where(Fichier_Final['Sous_Categorie'].isnull(), Fichier_Final['Categorie'],
                                           Fichier_Final['Sous_Categorie'])

Fichier_Final = Fichier_Final[
    ['jours_extraction', 'Nom_Fichier', 'Categorie', 'code SI', 'Sous_Categorie', 'Montant en Euros']]

Fichier_Final.to_csv(localOutput + "/1_Fichier_recap.csv", sep=';', encoding='UTF-8', index=False, decimal=',')

print('generation ' + localOutput + '/1_Fichier_recap.csv ')

print('fin python')
