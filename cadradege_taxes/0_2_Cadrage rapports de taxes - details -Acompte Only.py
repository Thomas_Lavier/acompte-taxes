# -------------------------
# --- Detect problems
# -------------------------

import glob
import pandas as pd
import numpy as np
import datetime
import re
import Constant

repo = Constant.repo()
localOutput = Constant.localOutput()
list_ppi = Constant.list_ppi_file()

# Liste des fichiers à comparer - Input du script - on va investiguer tous les Acompte_Details
Files = glob.glob(repo+'/202*/Export_Acompte_Details_20*.csv', recursive=True)

# Créer DF Vide - Fichier_Final
column_names = ["Transaction Number"]
Fichier_Final = pd.DataFrame(columns=column_names)
Fichier_Final['Total_Mensuel'] = 0

# Date du tag pour investigation
# Demander confirmation à Flavien si ce tag fait bien parti du JSON qui se load
# dans le process boomi update with file
Date_tag = '2016-1-1'

# Pour chaque fichier type Acompte_Details
for X_Files in Files:
	print(X_Files)   
	# Travail sur le Chemin/Date/Fichier - url
	url = X_Files.split("\\")
	Date_repertoire = url[1]
	Nom_Fichier = url[2]
	print(Date_repertoire)
	Date_repertoire_date = datetime.datetime.strptime(Date_repertoire, '%Y-%m-%d')

	Split_nom_fichier = re.split('Export_|_202', Nom_Fichier)
	Categorie = Split_nom_fichier[1]
	print(Categorie)

	# Connaitre la dernière génération pour le calcul des tags
	if Date_repertoire_date > datetime.datetime.strptime(Date_tag, '%Y-%m-%d'):
		Date_tag = Date_repertoire
	else:
		Date_tag
	print(Date_tag)

	# Travail sur les fichiers

	if Categorie == 'Acompte_Details':
		Export = pd.read_csv(X_Files, sep=';', encoding='UTF-8')[['Transaction Number', 'Montant en Euros']]
		Export = Export.groupby(['Transaction Number'], as_index=False).sum()
		Export = Export.rename(columns={"Montant en Euros": "Mensu_"+Date_repertoire})

		# Merge + Création du Total Mensuel
		Fichier_Final = Fichier_Final.merge(Export, on='Transaction Number', how='outer').fillna(0)
		Fichier_Final['Total_Mensuel'] = Fichier_Final['Total_Mensuel'] + Fichier_Final["Mensu_"+Date_repertoire]
		Fichier_Final["Mensu_"+Date_repertoire] = Fichier_Final["Mensu_"+Date_repertoire].round(2)
	else:
		print('ERROR' + url)

Fichier_Final['Total_Mensuel'] = Fichier_Final['Total_Mensuel'].round(2)

Fichier_Final["Tag"+Date_tag] = np.where(Fichier_Final["Mensu_"+Date_tag] > 0, 'TRUE', 'None')
Fichier_Final["Tag"+Date_tag] = np.where(Fichier_Final["Mensu_"+Date_tag] < 0, 'FALSE', Fichier_Final["Tag"+Date_tag])

Fichier_Final["Tag_Total"] = np.where(Fichier_Final["Total_Mensuel"] > 0, 'TRUE', 'None')
Fichier_Final["Tag_Total"] = np.where(Fichier_Final["Total_Mensuel"] < 0, 'FALSE', Fichier_Final["Tag_Total"])
# Quand =0 donne None, None veut dire que ça n'a pas bougé donc il n'y a pas eu d'évolution sur tout le mois
# Car aucun paiment ou rejet

# A MAJ Chaque mois, TC liste des ppi.id et des ppic.id
# extract_ppi_zuora = glob.glob(repo+'/liste ppi/1_Fichier_recap_ppi.csv')
extract_ppi_zuora = glob.glob(list_ppi +'/1_Fichier_recap_ppi.csv')
print(extract_ppi_zuora)

# extract_ppi_zuora[0] on ne récupére que le premier fichier dans le glob
# Car il n'y en a qu'un: /liste ppi/1_Fichier_recap_ppi.csv
# extract_ppi_zuora = pd.read_csv(extract_ppi_zuora[0], sep=';', encoding='UTF-8')[['ppicId', 'id']]
extract_ppi_zuora = pd.read_csv(extract_ppi_zuora[0], sep=',', encoding='UTF-8')[['ppicId', 'id']]
extract_ppi_zuora = extract_ppi_zuora.rename(columns={'id': 'Transaction Number'})

# ICI pourquoi on merge ou Join ?
# Pourquoi un left et pas un inner join?
Fichier_Final = Fichier_Final.merge(extract_ppi_zuora, on='Transaction Number', how='left')

# Génération tous les PPI par mois/trimestre + total mois/trim
Fichier_Final.to_csv(localOutput + "/2_Fichier_Final_Acompte_Full.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
print('generation ' + localOutput + '/2_Fichier_Final_Acompte_Full.csv ')


########################################################################################3
# Génération des fichiers de tag & contrôle MENSUEL
Fichier_tag_mensuel_origin = Fichier_Final.loc[ Fichier_Final["Tag"+Date_tag] != 'None' ,:]
Fichier_tag_mensuel = Fichier_tag_mensuel_origin[['ppicId',"Tag"+Date_tag]]
Fichier_tag_mensuel = Fichier_tag_mensuel.rename(columns={"Tag"+Date_tag:"TrasferredTOAccountingLastMonth__c"})

#### Si ppic n est pas dans "extract_ppi_zuora" MENSUEL ne sert que pour le TAG
Fichier_tag_error_mensuel = Fichier_tag_mensuel_origin.loc[Fichier_tag_mensuel_origin["ppicId"].isnull() ,:]
Fichier_tag_error_mensuel.to_csv(localOutput + "/2_Fichier_Final_Acompte_Full_Plan_taggage_Error_ppic.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
print('generation ' + localOutput +'/2_Fichier_Final_Acompte_Full_Plan_taggage_Error_ppic.csv ')

Fichier_tag_mensuel.to_csv(localOutput + "/2_Fichier_Final_Acompte_Full_Plan_taggage_Mensuel.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
print('generation ' + localOutput + '/2_Fichier_Final_Acompte_Full_Plan_taggage_Mensuel.csv ')

########################################################################################3
#### Génération des fichiers de tag & contrôle QUARTER
Fichier_tag_trimestriel_origin = Fichier_Final.loc[ Fichier_Final["Tag_Total"] != 'None' ,:]
Fichier_tag_trimestriel = Fichier_tag_trimestriel_origin[['ppicId',"Tag_Total"]]
Fichier_tag_trimestriel = Fichier_tag_trimestriel.rename(columns={"Tag_Total":"TrasferredTOAccountingLast3Month__c"})

#### Si ppic n est pas dans "extract_ppi_zuora" QUARTER
Fichier_tag_error_trimestriel = Fichier_tag_trimestriel_origin.loc[Fichier_tag_trimestriel_origin["ppicId"].isnull() ,:]
Fichier_tag_error_trimestriel.to_csv(localOutput + "/2_Fichier_Final_Acompte_Full_Plan_taggage_Error_ppic_Trimestriel.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
print('generation ' + localOutput + '/2_Fichier_Final_Acompte_Full_Plan_taggage_Error_ppic_Trimestriel.csv ')

Fichier_tag_trimestriel.to_csv(localOutput + "/2_Fichier_Final_Acompte_Full_Plan_taggage_Trimestriel.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
print('generation ' + localOutput + '/2_Fichier_Final_Acompte_Full_Plan_taggage_Trimestriel.csv ')


print('fin python')


