# -------------------------
# --- Find gaps if problems
# -------------------------

import glob
import pandas as pd
import numpy as np
import datetime
import re
import Constant

repo = Constant.repo()
localOutput = Constant.localOutput()

### Liste des fichiers à comparer
Files_Acompte = glob.glob(repo+'/202*/Export_Acompte_Details_20*.csv', recursive=True)
Files_CTA_ELEC = glob.glob(repo+'/202*/Export_CTA_ELEC_20*.csv', recursive=True)
Files_CTA_GAZ = glob.glob(repo+'/202*/Export_CTA_GAZ_20*.csv', recursive=True)

Files_CSPE = glob.glob(repo+'/202*/Export_CSPE_20*.csv', recursive=True)
Files_TICGN = glob.glob(repo+'/202*/Export_TICGN_20*.csv', recursive=True)

Files = Files_Acompte + Files_CSPE + Files_TICGN + Files_CTA_ELEC + Files_CTA_GAZ

## Créer DF Vide
column_names = ["Transaction Number"]
Fichier_Final_CSPE = pd.DataFrame(columns = column_names)
Fichier_Final_CSPE['Total_Mensuel'] = 0
Fichier_Final_CSPE['Total_Trim'] = 0

Fichier_Final_TICGN = pd.DataFrame(columns = column_names)
Fichier_Final_TICGN['Total_Mensuel'] = 0
Fichier_Final_TICGN['Total_Trim'] = 0

Fichier_Final_CTA_GAZ = pd.DataFrame(columns = column_names)
Fichier_Final_CTA_GAZ['Total_Mensuel'] = 0
Fichier_Final_CTA_GAZ['Total_CTA_GAZ'] = 0

Fichier_Final_CTA_ELEC = pd.DataFrame(columns = column_names)
Fichier_Final_CTA_ELEC['Total_Mensuel'] = 0
Fichier_Final_CTA_ELEC['Total_CTA_ELEC'] = 0

for X_Files in Files:
	print(X_Files)   
	##### Travail sur le Chemin/Date/Fichier
	url = X_Files.split("\\")
	Date_repertoire = url[1]
	Nom_Fichier = url[2]
	print(Date_repertoire)
	Split_nom_fichier = re.split('Export_|_202',Nom_Fichier)
	Categorie = Split_nom_fichier[1]
	print(Categorie)
    
    
    ### Travail sur les fichiers

	if Categorie == 'Acompte_Details' :
		Export = pd.read_csv(X_Files, sep=';', encoding='UTF-8')[['Transaction Number','Nom du compte','Montant en Euros']]
		Export = Export.groupby(['Transaction Number','Nom du compte'], as_index=False).sum()
		Export = Export.rename(columns={"Montant en Euros":"Mensu_"+Date_repertoire})

		Export_CSPE = Export.loc[Export['Nom du compte'] == 'CSPE' ,:]
		Export_CSPE = Export_CSPE.drop(columns=['Nom du compte'])

		Export_TICGN = Export.loc[Export['Nom du compte'] == 'TICGN' ,:]
		Export_TICGN = Export_TICGN.drop(columns=['Nom du compte'])

		Export_CTA_GAZ = Export.loc[Export['Nom du compte'] == 'CTA Gaz' ,:]
		Export_CTA_GAZ = Export_CTA_GAZ.drop(columns=['Nom du compte'])

		Export_CTA_ELEC = Export.loc[Export['Nom du compte'] == 'CTA Elec' ,:]
		Export_CTA_ELEC = Export_CTA_ELEC.drop(columns=['Nom du compte'])

    ### Merge + Création du Total Mensuel
		Fichier_Final_CSPE = Fichier_Final_CSPE.merge(Export_CSPE, on='Transaction Number', how='outer').fillna(0)
		Fichier_Final_CSPE['Total_Mensuel'] = Fichier_Final_CSPE['Total_Mensuel'] + Fichier_Final_CSPE["Mensu_"+Date_repertoire]
#		print(Fichier_Final_TICGN)   
		Fichier_Final_TICGN = Fichier_Final_TICGN.merge(Export_TICGN, on='Transaction Number', how='outer').fillna(0)
		Fichier_Final_TICGN['Total_Mensuel'] = Fichier_Final_TICGN['Total_Mensuel'] + Fichier_Final_TICGN["Mensu_"+Date_repertoire]

		Fichier_Final_CTA_GAZ = Fichier_Final_CTA_GAZ.merge(Export_CTA_GAZ, on='Transaction Number', how='outer').fillna(0)
		Fichier_Final_CTA_GAZ['Total_Mensuel'] = Fichier_Final_CTA_GAZ['Total_Mensuel'] + Fichier_Final_CTA_GAZ["Mensu_"+Date_repertoire]

		Fichier_Final_CTA_ELEC = Fichier_Final_CTA_ELEC.merge(Export_CTA_ELEC, on='Transaction Number', how='outer').fillna(0)
		Fichier_Final_CTA_ELEC['Total_Mensuel'] = Fichier_Final_CTA_ELEC['Total_Mensuel'] + Fichier_Final_CTA_ELEC["Mensu_"+Date_repertoire]

	elif Categorie == 'CSPE':
		Export = pd.read_csv(X_Files, sep=',', encoding='UTF-8')[['code SI','Transaction Number','Montant en Euros']]
        ## Selection partie acompte
		Export = Export.loc[ Export['code SI'] == 'Acompte' ,:]
		Export = Export.drop(columns=['code SI'])
        ## Nommage de la colonne trimestre en fonction de la date de génération du fichier
		Export = Export.rename(columns={"Montant en Euros":"Trim_"+Date_repertoire})
		Export = Export.groupby(['Transaction Number'], as_index=False).sum()

		Fichier_Final_CSPE = Fichier_Final_CSPE.merge(Export, on='Transaction Number', how='outer').fillna(0)
		Fichier_Final_CSPE['Total_Trim'] = Fichier_Final_CSPE['Total_Trim'] + Fichier_Final_CSPE["Trim_"+Date_repertoire]
  
	elif Categorie == 'TICGN':
		Export = pd.read_csv(X_Files, sep=',', encoding='UTF-8')[['code SI','Transaction Number','Montant en Euros']]
        ## Selection partie acompte
		Export = Export.loc[ Export['code SI'] == 'Acompte' ,:]
		Export = Export.drop(columns=['code SI'])
        ## Nommage de la colonne trimestre en fonction de la date de génération du fichier
		Export = Export.rename(columns={"Montant en Euros":"Trim_"+Date_repertoire})
		Export = Export.groupby(['Transaction Number'], as_index=False).sum()

		Fichier_Final_TICGN = Fichier_Final_TICGN.merge(Export, on='Transaction Number', how='outer').fillna(0)
		Fichier_Final_TICGN['Total_Trim'] = Fichier_Final_TICGN['Total_Trim'] + Fichier_Final_TICGN["Trim_"+Date_repertoire]

	elif Categorie == 'CTA_GAZ':
		Export = pd.read_csv(X_Files, sep=',', encoding='UTF-8')[['code SI','Transaction Number','Montant en Euros']]
        ## Selection partie acompte
		Export = Export.loc[ Export['code SI'] == 'Acompte' ,:]
		Export = Export.drop(columns=['code SI'])
        ## Nommage de la colonne trimestre en fonction de la date de génération du fichier
		Export = Export.rename(columns={"Montant en Euros":"CTA_GAZ_"+Date_repertoire})
		Export = Export.groupby(['Transaction Number'], as_index=False).sum()

		Fichier_Final_CTA_GAZ = Fichier_Final_CTA_GAZ.merge(Export, on='Transaction Number', how='outer').fillna(0)
		Fichier_Final_CTA_GAZ['Total_CTA_GAZ'] = Fichier_Final_CTA_GAZ['Total_CTA_GAZ'] + Fichier_Final_CTA_GAZ["CTA_GAZ_"+Date_repertoire]


	elif Categorie == 'CTA_ELEC':
		Export = pd.read_csv(X_Files, sep=',', encoding='UTF-8')[['code SI','Transaction Number','Montant en Euros']]
        ## Selection partie acompte
		Export = Export.loc[ Export['code SI'] == 'Acompte' ,:]
		Export = Export.drop(columns=['code SI'])
        ## Nommage de la colonne trimestre en fonction de la date de génération du fichier
		Export = Export.rename(columns={"Montant en Euros":"CTA_ELEC_"+Date_repertoire})
		Export = Export.groupby(['Transaction Number'], as_index=False).sum()

		Fichier_Final_CTA_ELEC = Fichier_Final_CTA_ELEC.merge(Export, on='Transaction Number', how='outer').fillna(0)
		Fichier_Final_CTA_ELEC['Total_CTA_ELEC'] = Fichier_Final_CTA_ELEC['Total_CTA_ELEC'] + Fichier_Final_CTA_ELEC["CTA_ELEC_"+Date_repertoire]

	else :
		print('ERROR' + url )

### Génération tous les PPI par mois/trimestre + total mois/trim        
Fichier_Final_CSPE.to_csv(localOutput + "/3_Fichier_Final_CSPE_Full.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
Fichier_Final_TICGN.to_csv(localOutput + "/3_Fichier_Final_TICGN_Full.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
Fichier_Final_CTA_GAZ.to_csv(localOutput + "/3_Fichier_Final_CTA_GAZ_Full.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
Fichier_Final_CTA_ELEC.to_csv(localOutput + "/3_Fichier_Final_CTA_ELEC_Full.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
print('generation ' + localOutput + '/3_Fichier_Final_XXX_Full.csv ')

### Génération fichier avec ppi en écarts
Fichier_Final_CSPE = Fichier_Final_CSPE.loc[Fichier_Final_CSPE['Total_Trim'] != Fichier_Final_CSPE['Total_Mensuel'] ,:]
Fichier_Final_TICGN = Fichier_Final_TICGN.loc[Fichier_Final_TICGN['Total_Trim'] != Fichier_Final_TICGN['Total_Mensuel'] ,:]
Fichier_Final_CTA_GAZ = Fichier_Final_CTA_GAZ.loc[Fichier_Final_CTA_GAZ['Total_CTA_GAZ'] != Fichier_Final_CTA_GAZ['Total_Mensuel'] ,:]
Fichier_Final_CTA_ELEC = Fichier_Final_CTA_ELEC.loc[Fichier_Final_CTA_ELEC['Total_CTA_ELEC'] != Fichier_Final_CTA_ELEC['Total_Mensuel'] ,:]

Fichier_Final_CSPE.to_csv(localOutput + "/3_Fichier_Final_CSPE_Ecarts.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
Fichier_Final_TICGN.to_csv(localOutput + "/3_Fichier_Final_TICGN_Ecarts.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
Fichier_Final_CTA_GAZ.to_csv(localOutput + "/3_Fichier_Final_CTA_GAZ_Ecarts.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
Fichier_Final_CTA_ELEC.to_csv(localOutput + "/3_Fichier_Final_CTA_ELEC_Ecarts.csv", sep=';', encoding='UTF-8', index=False, decimal=',')
print('generation ' + localOutput + '/3_Fichier_Final_XXX_Ecarts.csv ')